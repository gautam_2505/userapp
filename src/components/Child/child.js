import React from 'react';
import { connect } from 'react-redux';

const mapStateToProps = state => {
  return {
    userNameEntered: state.user.userNameEntered
  };
};

const Child = props => {
  return ( 
    <h3>{props.userNameEntered}</h3>
  )
};

export default connect(
  mapStateToProps,
  null
)(Child);