import React, { Component } from 'react';

const Toolbar = props => {
  return ( 
    <header style= {{backgroundColor: '#2F4F4F'}}>
      <nav>
        <button onClick={props.drawerClickHandler} style={{margin: '0.5rem'}}>Sidedrawer</button>
      </nav>
    </header>
  )
};

export default Toolbar;