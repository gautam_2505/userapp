import React, { useState } from 'react';
import { connect } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { currentUser } from '../../actions/userActions';


const mapStateToProps = state => {
  return {
     userEmail: state.user.email,
  };
};

const mapDipatchToProps = dispatch => {
  return {
    currentUserInfo: (email) => dispatch(currentUser(email)),
  };
};

const LoginPage = (props) => {
  const [email, setEmail]= useState('');


  let history = useHistory();

  const onInputHandleChange = (e) => {
    setEmail(e.target.value);
  }

  const onSubmit = () => {
    console.log("Submit clicked...");
    // Updating the store with new values in same component..
      props.currentUserInfo(email);
      localStorage.setItem('email', email);
    // history.push() should be the last stmt...
    history.push('/dashboard');
    }
  
  
  return (    
    <div style={{backgroundColor: "#DCDCDC"}}>
      Email: <input type="text" value = {email} onChange = {(e) => {onInputHandleChange(e)}}></input><br></br><br></br>
      Pswd: <input></input><br></br><br></br>
      <button onClick={onSubmit}>SignIn</button> 
    </div>
  );
}

export default connect(
  mapStateToProps,
  mapDipatchToProps
)(LoginPage);