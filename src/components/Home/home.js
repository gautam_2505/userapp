import React, { useState } from 'react';
import { connect } from 'react-redux';
import { useHistory, Route, Switch } from 'react-router-dom';
import { currentUser } from '../../actions/userActions';
import Dashboard from '../Dashboard/dashboard';
import Toolbar from '../Toolbar/Toolbar';
import SideDrawer from '../SideDrawer/SideDrawer';
import Content from './Content';
import LoginPage from './loginPage';


const mapStateToProps = state => {
  return {
     userEmail: state.user.email,
  };
};

const mapDipatchToProps = dispatch => {
  return {
    currentUserInfo: (email) => dispatch(currentUser(email)),
  };
};

const Home = (props) => {
  const [email, setEmail]= useState('');
  const [sideDrawerOpen, setsideDrawerOpen]= useState(false);


  let history = useHistory();

  const onInputHandleChange = (e) => {
    setEmail(e.target.value);
  }

  const onSubmit = () => {
    console.log("Submit clicked...");
    // Updating the store with new values in same component..
      props.currentUserInfo(email);
      localStorage.setItem('email', email);
    // history.push() should be the last stmt...
    history.push('/dashboard');
    }

  let sideDrawer;

  if(sideDrawerOpen){
    console.log('sideDrawer===[>>>', sideDrawer);
    sideDrawer = <SideDrawer />
  }
  
  return (    
    <div style={{display: "flex", backgroundColor: "#DCDCDC"}}>
      <Content email={email} component={LoginPage}/>
    </div>
  );
}

export default connect(
  mapStateToProps,
  mapDipatchToProps
)(Home);