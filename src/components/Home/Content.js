import React from 'react';
import { Redirect } from 'react-router-dom';

// This component is used to wrap any components that should be protected via authentication
export default ({ component: Component, ...rest }) => {

    return (
      <div>
        <Component {...rest} />
      </div>
    );
  }
 
