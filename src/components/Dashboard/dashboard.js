import React, { useState } from 'react';
import { Route } from 'react-router-dom';
import { connect } from 'react-redux';
import { useHistory } from 'react-router-dom';
import Child from '../Child/child';
import { newUserEntered } from '../../actions/userActions';


const mapStateToProps = state => {
  return {
     email: state.user.email,
     userNameEntered: state.user.userNameEntered,
  };
};

const mapDipatchToProps = dispatch => {
  return {
    newUserInfo: (userNameEntered) => dispatch(newUserEntered(userNameEntered)),
  };
};

  const Dashboard = (props) => {
    let history = useHistory();
    const[userNameEntered, setUserNameEntered] = useState('');

    const onInputHandleChange = (e) => {
      setUserNameEntered(e.target.value);
    }
    
    const onUpdatedUserChange = (e) => {
      // var newUser = prompt("Please enter your name", props.email);
      // // var newUser = prompt("Please enter your name", props.newUserInfo(userNameEntered));
      // console.log("newUser", newUser);
      // setUserNameEntered(newUser);
      // // setUserNameEntered(e.target.value);
      // console.log('userNameEntered=======>>>>>', userNameEntered);
      props.newUserInfo(userNameEntered);
      // history.push('/child');
      // history.push('/child/:userNameEntered')
      history.push(`/child/:${userNameEntered}`);
    }

    return (
      <div>
        <h3>{props.email}</h3>
        <input type="text" value = {userNameEntered} onChange = {(e) => {onInputHandleChange(e)}}></input>
        <li onClick={(e) => onUpdatedUserChange(e)}>Updated User</li>
      </div>
    );
  }


  export default connect(
    mapStateToProps,
    mapDipatchToProps
  )(Dashboard);