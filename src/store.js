import { applyMiddleware, createStore } from 'redux';
import { createLogger } from 'redux-logger';
// import * as thunk from 'redux-thunk';
import thunk from 'redux-thunk';
import promise from 'redux-promise-middleware';
import { composeWithDevTools } from 'redux-devtools-extension/logOnlyInProduction';

import reducer from './reducers';

// const storeProd = createStore(reducer, composeWithDevTools(applyMiddleware()));
const storeLocal = createStore(
  reducer,
  composeWithDevTools(applyMiddleware(createLogger(), thunk))
);

export default storeLocal; // eslint-disable-line
