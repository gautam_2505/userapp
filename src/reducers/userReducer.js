export default function reducer(
  state = { email: '', userNameEntered: '' },
  action
) {
  switch (action.type) {
    
      case 'EMAIL':
      return {
        ...state,
        email: action.payload,
      };
      case 'USERNAMEENTERED':
      return {
        ...state,
        userNameEntered: action.payload,
      };
    default:
      return { ...state };
  }
}
