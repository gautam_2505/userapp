export function currentUser(email) {
  return dispatch => {
    dispatch({
      type: 'EMAIL',
      payload: email,
    });
  };
}

export function newUserEntered(userNameEntered) {
  return dispatch => {
    dispatch({
      type: 'USERNAMEENTERED',
      payload: userNameEntered,
    });
  };
}

