import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { BrowserRouter as Router, Link} from 'react-router-dom';
import { Route, Switch } from 'react-router-dom';
// import { currentUser } from './actions/userActions';
import  Dashboard  from './components/Dashboard/dashboard';
import  Home  from './components/Home/home';
import Child from'./components/Child/child';
import Toolbar from './components/Toolbar/Toolbar';
import SideDrawer from './components/SideDrawer/SideDrawer';


  const mapStateToProps = state => {
    return {
       email: state.user.email,
       userNameEntered: state.user.userNameEntered
    };
  };
  // const mapDipatchToProps = dispatch => {
  //   return {
  //     currentUserInfo: (email) => dispatch(currentUser(email)),
  //   };
  // };

  const App = (props) => {

    const[isLoggedIn, setIsLoggedIn] = useState(true);
    const [sideDrawerOpen, setsideDrawerOpen]= useState(false);

    useEffect(() => {
      localStorage.getItem('email') ? setIsLoggedIn(true) : setIsLoggedIn(false);
    }, []);

    const sideDrawerOpenHandler = () => {
      setsideDrawerOpen(!sideDrawerOpen); 
    }

  return (
    <div style={{display: "flex"}}>
    <Toolbar drawerClickHandler={sideDrawerOpenHandler} />
    { sideDrawerOpen ?  <SideDrawer /> : null }
    <Router>
      <div className="App">
        <Switch>    
        <Route path='/' exact render = {
          () => {
            return isLoggedIn ? <Dashboard email={props.email} /> 
            : <Home /> 
          }
        }/>

        <Route exact path='/logout' render = {
                  () => {
                    localStorage.clear();
                    return (
                      <Home />
                    )
                  }
                } />

        <Route path='/dashboard' render = {
          () => {
            return <Dashboard email={props.email}/>
          }
         } />

         <Route 
          exact 
          // path='/child'
          path={`/child/:${props.userNameEntered}`} 
          render = { 
            () => {
              return <Child userNameEntered={props.userNameEntered}/>
            }
        } />
        </Switch>
      </div>
    </Router>
    </div>
  );
}

export default connect(
  mapStateToProps,
  null
)(App);